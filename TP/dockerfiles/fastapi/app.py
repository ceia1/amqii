import json
import pickle
import boto3
import mlflow

import numpy as np
import pandas as pd

from typing import Literal
from fastapi import FastAPI, Body, BackgroundTasks
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field
from typing_extensions import Annotated

def load_model(model_name: str, alias: str):
    """
    Load a trained model and associated data dictionary.

    This function attempts to load a trained model specified by its name and alias. If the model is not found in the
    MLflow registry, it loads the default model from a file. Additionally, it loads information about the ETL pipeline
    from an S3 bucket. If the data dictionary is not found in the S3 bucket, it loads it from a local file.

    :param model_name: The name of the model.
    :param alias: The alias of the model version.
    :return: A tuple containing the loaded model, its version, and the data dictionary.
    """

    try:
        mlflow.set_tracking_uri('http://mlflow:5000')
        client_mlflow = mlflow.MlflowClient()

        model_data_mlflow = client_mlflow.get_model_version_by_alias(model_name, alias)
        model_ml = mlflow.sklearn.load_model(model_data_mlflow.source)
        version_model_ml = int(model_data_mlflow.version)

    except:
        # Si no hay registro en MLflow, abre el modelo predeterminado
        file_ml = open('/app/files/model.pkl', 'rb')
        model_ml = pickle.load(file_ml)
        file_ml.close()
        version_model_ml = 0

    return model_ml, version_model_ml

def check_model():
    """
    Check for updates in the model and update if necessary.

    The function checks the model registry to see if the version of the champion model has changed. If the version
    has changed, it updates the model and the data dictionary accordingly.

    :return: None
    """

    global model
    global version_model

    try:
        model_name = "diabetes_prediction_model_prod"
        alias = "champion"

        mlflow.set_tracking_uri('http://mlflow:5000')
        client = mlflow.MlflowClient()

        # Check in the model registry if the version of the champion has changed
        new_model_data = client.get_model_version_by_alias(model_name, alias)
        new_version_model = int(new_model_data.version)

        # If the versions are not the same
        if new_version_model != version_model:
            # Load the new model and update version and data dictionary
            model, version_model = load_model(model_name, alias)

    except:
        # If an error occurs during the process, pass silently
        pass

class ModelInput(BaseModel):
    """
    Input schema for the diabetes prediction model.

    This class defines the input fields required by the diabetes prediction model along with their descriptions
    and validation constraints.

    :param Pregnancies: number of times pregnant (0 to 15)
    :param Glucose: plasma glucose concentration a 2 hours in an oral glucose tolerance test (1 to 1000)
    :param BloodPressure: diastolic blood pressure (mm Hg) (1 to 200)
    :param SkinThickness: triceps skin fold thickness (mm) (1 to 50)
    :param Insulin: 2-Hour serum insulin (mu U/ml) (1 to 200)
    :param BMI: body mass index (weight in kg/(height in m)^2) (0.1 to 70.0)
    :param DiabetesPedigreeFunction: diabetes pedigree function (0.001 to 50.0)
    :param Age: age of the patient in years (0 to 130)
    """

    Pregnancies: int = Field(
        description="Number of times pregnant",
        ge=0,
        le=15,
    )
    Glucose: int = Field(
        description="Plasma glucose concentration a 2 hours in an oral glucose tolerance test",
        ge=1,
        le=1000,
    )
    BloodPressure: int = Field(
        description="Diastolic blood pressure (mm Hg)",
        ge=1,
        le=200,
    )
    SkinThickness: int = Field(
        description="Triceps skin fold thickness (mm)",
        ge=1,
        le=50,
    )
    Insulin: int = Field(
        description="2-Hour serum insulin (mu U/ml)",
        ge=1,
        le=200,
    )
    BMI: float = Field(
        description="body mass index (weight in kg/(height in m)^2)",
        ge=0.1,
        le=70.0,
    )
    DiabetesPedigreeFunction: float = Field(
        description="Diabetes pedigree function",
        ge=0.001,
        le=50.0,
    )
    Age: int = Field(
        description="Age of the patient in years",
        ge=0,
        le=130,
    )
    
    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "Pregnancies": 0,
                    "Glucose": 140,
                    "BloodPressure": 70,
                    "SkinThickness": 15,
                    "Insulin": 15,
                    "BMI": 20.0,
                    "DiabetesPedigreeFunction": 2.0,
                    "Age": 30,
                }
            ]
        }
    }

class ModelOutput(BaseModel):
    """
    Output schema for the diabetes prediction model.

    This class defines the output fields returned by the diabetes prediction model along with their descriptions
    and possible values.

    :param int_output: Output of the model. True if the patient has diabetes.
    :param str_output: Output of the model in string form. Can be "Healthy patient" or "Diabetes detected".
    """

    int_output: bool = Field(
        description="Output of the model. True if the patient has diabetes",
    )
    str_output: Literal["Healthy patient", "Diabetes detected"] = Field(
        description="Output of the model in string form",
    )

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "int_output": True,
                    "str_output": "Diabetes detected",
                }
            ]
        }
    }

# Load the model before start
model, version_model = load_model("diabetes_prediction_model_prod", "champion")

app = FastAPI()

@app.get("/")
async def read_root():
    """
    Root endpoint of the Diabetes Detector API.

    This endpoint returns a JSON response with a welcome message to indicate that the API is running.
    """
    return JSONResponse(content=jsonable_encoder({"message": "Welcome to the Diabetes Detector API"}))

@app.post("/predict/", response_model=ModelOutput)
def predict(
    features: Annotated[
        ModelInput,
        Body(embed=True),
    ],
    background_tasks: BackgroundTasks
):
    """
    Endpoint for predicting diabetes.

    This endpoint receives features related to a patient's health and predicts whether the patient has diabetes
    or not using a trained model. It returns the prediction result in both integer and string formats.
    """

    # Extract features from the request and convert them into a list and dictionary
    features_list = [*features.dict().values()]
    features_key = [*features.dict().keys()]

    # Convert features into a pandas DataFrame
    features_df = pd.DataFrame(np.array(features_list).reshape([1, -1]), columns=features_key)

    # Make the prediction using the trained model
    prediction = model.predict(features_df)

    # Convert prediction result into string format
    str_pred = "Healthy patient"
    if prediction[0] > 0:
        str_pred = "Diabetes detected"

    # Check if the model has changed asynchronously
    background_tasks.add_task(check_model)

    # Return the prediction result
    return ModelOutput(int_output=bool(prediction[0].item()), str_output=str_pred)
