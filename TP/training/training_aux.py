import pandas as pd

def set_environment_parameters() -> str:
    """
    Sets environment parameters for AWS and MLflow.

    The following environment variables are set:
    - AWS_ACCESS_KEY_ID: 
                        specifies the access key ID for authentication 
                        with the storage service.
    - AWS_SECRET_ACCESS_KEY: 
                        specifies the secret access key for authentication
                        with the storage service.
    - MLFLOW_S3_ENDPOINT_URL: 
                        specifies the endpoint URL for MLflow to 
                        interact with the S3-compatible storage service.
    - AWS_ENDPOINT_URL_S3: 
                        specifies the endpoint URL for the AWS S3-compatible 
                        storage service.

    Returns:
        A message indicating that the environment has been properly configured.
    """

    import os

    os.environ['AWS_ACCESS_KEY_ID'] = 'minio'
    os.environ['AWS_SECRET_ACCESS_KEY'] = 'minio123'
    os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'http://localhost:9000'
    os.environ['AWS_ENDPOINT_URL_S3'] = 'http://localhost:9000'

    return 'Properly configured environment.'

def read_training_dataset() -> pd.DataFrame:
    """
    Reads the training dataset from an S3 bucket.

    Returns:
        pd.DataFrame: A DataFrame containing the training dataset.
    """
   
    import awswrangler as wr

    df = wr.s3.read_parquet("s3://data/diabetes_data.parquet")

    return df

def split_dataset(df: pd.DataFrame, test_size: float):
    """
    Splits the dataset into training and testing sets.

    Args:
        df (pd.DataFrame): 
                        The DataFrame to be split, containing both 
                        features and the target column.
        test_size (float): 
                        The proportion of the dataset to include 
                        in the test split.

    Returns:
        tuple: A tuple containing four elements:
            - x_train (pd.DataFrame): the training features.
            - x_test (pd.DataFrame): the testing features.
            - y_train (pd.Series): the training target.
            - y_test (pd.Series): the testing target.
    """

    import pandas as pd
    from sklearn.model_selection import train_test_split

    x = df.drop(['Outcome', 'Timestamp'], axis=1)
    y = df.Outcome

    x_train, x_test, y_train, y_test = train_test_split(
                        x, y, test_size=test_size, shuffle=True, stratify=y
    )

    return x_train, x_test, y_train, y_test

def set_mlflow():
    """
    Sets up an MLflow experiment and returns the experiment ID and run name.

    Returns:
        tuple: A tuple containing two elements:
            - experiment_id (str): 
                                the ID of the experiment.
            - run_name_parent (str): 
                                the name of the run parent, 
                                which includes a timestamp.
    """
    
    import mlflow
    import datetime

    # Set MLflow tracking URI
    mlflow_server = "http://localhost:5000"
    mlflow.set_tracking_uri(mlflow_server)

    # Create or retrieve experiment
    experiment = mlflow.get_experiment_by_name("Diabetes Existence Prediction")
    if experiment:
        experiment_id = experiment.experiment_id
    else:
        experiment_id = mlflow.create_experiment(
                                "Diabetes Existence Prediction"
                            )

    # Generate run name with current date and time
    run_name_parent = "best_hyperparam_" + datetime.datetime.today().strftime('%Y/%m/%d-%H:%M:%S"')

    return experiment_id, run_name_parent

def train_retrain_log(
    experiment_id, run_name_parent, 
    x_train, x_test, y_train, y_test
    ):
    """
    Trains or retrains and logs a diabetes prediction model using MLflow.

    F-Beta_score is used as metric with a beta value > 1, in order to avoid
    type 2 errors (false negatives), which would be a serious mistake given
    the nature of this problem.

    Args:
        experiment_id (str): 
                    the ID of the MLflow experiment to log the run under.
        run_name_parent (str): 
                    the name of the parent run.
        x_train (pd.DataFrame): 
                    the training features.
        x_test (pd.DataFrame): 
                    the testing features.
        y_train (pd.Series): 
                    the training target.
        y_test (pd.Series): 
                    the testing target.

    Returns:
        tuple: A tuple containing four elements:
            - best_params (dict): 
                    the best hyperparameters found during model tuning.
            - model_name (str): 
                    the name of the trained model class.
            - fbeta (float): 
                    the F-beta score of the model on the test data.
            - model_uri (str): 
                    the URI of the logged model in the MLflow tracking server.
    """

    import numpy as np

    from sklearn.preprocessing import StandardScaler
    from sklearn.decomposition import PCA
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.metrics import make_scorer, fbeta_score
    from sklearn.model_selection import RandomizedSearchCV
    from sklearn.pipeline import Pipeline

    from mlflow.models import infer_signature
    import mlflow.sklearn

    with mlflow.start_run(
                        experiment_id=experiment_id, 
                        run_name=run_name_parent, 
                        nested=True
                        ):

        scaler = StandardScaler()
        x_train_scaled = scaler.fit_transform(x_train)
        x_test_scaled = scaler.transform(x_test)

        pca = PCA(0.9)
        x_train_pca = pca.fit_transform(x_train_scaled)
        x_test_pca = pca.transform(x_test_scaled)

        RFC = RandomForestClassifier(n_estimators=100, class_weight="balanced")

        custom_scorer = make_scorer(fbeta_score, beta=5)

        RFC_model = RandomizedSearchCV(
            RFC,
            {
                "max_depth": [int(i) for i in np.linspace(2, 25, 24)],
                "min_samples_split": [2, 3, 4],
                "min_samples_leaf": [2, 3, 4],
            },
            refit=True,
            scoring=custom_scorer,
            n_iter=100,
        )

        RFC_model.fit(x_train_pca, y_train)

        # Create a pipeline with scaler, PCA, and the model
        pipeline = Pipeline([
            ("scaler", scaler),
            ("pca", pca),
            ("model", RFC_model)
        ])

        y_pred = RFC_model.predict(x_test_pca)

        fbeta = fbeta_score(y_test, y_pred, beta=5)

        mlflow.log_metric("test_fbeta", fbeta)
        mlflow.log_params(RFC_model.best_params_)

        artifact_path = "model"
        signature = infer_signature(x_train, pipeline.predict(x_train))

        mlflow.sklearn.log_model(
            sk_model=pipeline,
            artifact_path=artifact_path,
            signature=signature,
            serialization_format='cloudpickle',
            registered_model_name="diabetes_prediction_model_dev",
            metadata={"model_data_version": 1},
        )

        # Save parameters for subsequent registration of the model
        model_uri = mlflow.get_artifact_uri(artifact_path)
        best_params = RFC_model.best_params_
        model_name = type(RFC).__name__

    return best_params, model_name, fbeta, model_uri

def register_model(best_params, model_name, fbeta, model_uri) -> str:
    """
    Registers a machine learning model in the MLflow model registry.

    Args:
        best_params (dict): 
                the best hyperparameters found during model tuning.
        model_name (str): 
                the name of the trained model class.
        fbeta (float): 
                the F-beta score of the model on the test data.
        model_uri (str): 
                the URI of the logged model in the MLflow tracking server.

    Returns:
        str: a message indicating the model has been successfully registered.
    """
    from mlflow import MlflowClient

    client = MlflowClient()

    name = "diabetes_prediction_model_prod"
    desc = "This model predicts the existence or absence of diabetes."

    # Create registered model if it doesn't exist
    try:
        client.create_registered_model(name=name, description=desc)
    except:
        print('Model has been already created.')

    # Prepare tags with best parameters, model name, and F-beta score
    tags = best_params
    tags["model"] = model_name
    tags["fbeta-score"] = fbeta

    # Create a model version with tags and set it as the champion
    result = client.create_model_version(
        name=name,
        source=model_uri,
        run_id=model_uri.split("/")[-3],
        tags=tags,
    )

    # Set the registered model alias
    client.set_registered_model_alias(name, "champion", result.version)

    return "Model correctly registered!"

