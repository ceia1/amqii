import datetime

from airflow.decorators import dag, task

markdown_text = """
### ETL process for diabetes original data
This DAG loads [Diabetes Dataset from Kaggle](https://www.kaggle.com/datasets/mathchi/diabetes-data-set?select=diabetes.csv)
in .parquet format to an s3 bucket.
"""

default_args = {
    'owner': "Natanael Emir Ferrán",
    'depends_on_past': False,
    'schedule_interval': None,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
    'dagrun_timeout': datetime.timedelta(minutes=15)
}


@dag(
    dag_id="original_data_ETL",
    description="Loads diabetes original dataset.",
    doc_md=markdown_text,
    tags=["ETL", "Diabetes"],
    default_args=default_args,
    catchup=False
)

def original_data_ETL():

    @task.virtualenv(
        task_id="load_dataset"
    )
    def load_dataset():
        """
        This task loads diabetes dataset to an s3 bucket.
        """

        import pandas as pd
        import datetime
        
        import awswrangler as wr

        url = 'https://raw.githubusercontent.com/plotly/datasets/master/diabetes.csv'
        df = pd.read_csv(url)

        # Add Timestamp column to the dataset
        df['Timestamp'] = datetime.datetime.today().strftime('%Y/%m/%d-%H:%M:%S"')

        path = 's3://data/diabetes_data.parquet'
        path_raw = 's3://data/raw/diabetes_original_data.parquet'

        wr.s3.to_parquet(df=df, path=path)
        wr.s3.to_parquet(df=df, path=path_raw)

    load_dataset()

dag = original_data_ETL()