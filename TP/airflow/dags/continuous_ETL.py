import datetime

from airflow.decorators import dag, task

markdown_text = """
### Continuous ETL process for diabetes dataset
This DAG simulates a weekly ETL process which updates diabetes dataset. It works as follows: 
1. Generates synthetic data based on the original diabetes dataset.
2. Appends new data to the dataset that will be used to retrain the ML model. 
"""

default_args = {
    'owner': "Natanael Emir Ferrán",
    'depends_on_past': False,
    'schedule_interval': '@weekly',
    'retries': 3,
    'retry_delay': datetime.timedelta(minutes=5),
    'dagrun_timeout': datetime.timedelta(minutes=15)
}

@dag(
    dag_id="continuous_ETL",
    description="Generates new data and loads it the into diabetes dataset",
    doc_md=markdown_text,
    tags=["ETL", "Diabetes"],
    default_args=default_args,
    catchup=False
)

def continuous_ETL():
    
    @task.virtualenv(
        task_id="generate_data"
    )
    def generate_data():
        """
        This task generates new data from the original diabetes_data dataset.
        """

        import pandas as pd
        import numpy as np
        import datetime

        import awswrangler as wr

        from sdv.metadata import SingleTableMetadata
        from sdv.single_table import GaussianCopulaSynthesizer

        df = wr.s3.read_parquet("s3://data/diabetes_data.parquet")

        df.drop('Timestamp', axis=1, inplace=True)

        # Ensure data types integrity
        df = df.astype({
                        'Pregnancies': 'int64',
                        'Glucose': 'int64',
                        'BloodPressure': 'int64',
                        'SkinThickness': 'int64',
                        'Insulin': 'int64',
                        'Age': 'int64',
                        'Outcome': 'int64'
                        })

        metadata = SingleTableMetadata()

        metadata.detect_from_dataframe(df)

        # Random number of new rows each run
        num_samples = np.random.randint(100)

        synthesizer = GaussianCopulaSynthesizer(metadata)

        synthesizer.fit(df)

        synthetic_data = synthesizer.sample(num_samples)

        # Add Timestamp column to the dataset
        synthetic_data['Timestamp'] = datetime.datetime.today().strftime('%Y/%m/%d-%H:%M:%S"')

        path = 's3://data/new_data.parquet'

        wr.s3.to_parquet(df=synthetic_data, path=path)

    @task.virtualenv(
        task_id="load_data"
    )
    def load_data():
        """
        This task appends new data to the entire dataset.
        """    

        import pandas as pd

        import awswrangler as wr

        df1 = wr.s3.read_parquet("s3://data/diabetes_data.parquet")
        df2 = wr.s3.read_parquet("s3://data/new_data.parquet")

        df = pd.concat([df1, df2], axis=0)

        path = 's3://data/diabetes_data.parquet'

        wr.s3.to_parquet(df=df, path=path)

    generate_data() >> load_data()

dag = continuous_ETL()
